﻿using UnityEngine;
using System.Collections;

public class Manager : MonoBehaviour
{
    public static Manager Instance { get; set; }

    public Material playerMaterial;
    public Color[] playerColors = new Color[10];
    public GameObject[] playerTrails = new GameObject[10];

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance = this;
    }

    public int currentLevel = 0;        //Used when changing from menu to game scene
    public int menuFocus = 0;           //Used when entering the menu scene
}
