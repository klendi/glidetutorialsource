﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class GameScene : MonoBehaviour
{
    private CanvasGroup fadeGroup;
    private float fadeInDuration = 2f;
    public bool gameStarted;

    private void Start()
    {
        //Get the only CG in the scene
        fadeGroup = FindObjectOfType<CanvasGroup>();

        //Set the fade to full opacity
        fadeGroup.alpha = 1;

    }

    private void Update()
    {
        if (Time.timeSinceLevelLoad <= fadeInDuration)
        {
            //Init fade it
            fadeGroup.alpha = 1 - (Time.timeSinceLevelLoad / fadeInDuration);
        }
        //If the initial fade-in is completed, and the game hasn't been started yet
        else if(!gameStarted)
        {
            //Ensure fade is done
            fadeGroup.alpha = 0;
            gameStarted = true;
        }
    }

    public void CompleteLevel()
    {
        //Complete the level, and save the progress
        SaveManager.Instance.CompleteLevel(Manager.Instance.currentLevel);

        //Focus the level selection when we return to the menu scene
        Manager.Instance.menuFocus = 1;

        ExitScene();
    }

    public void ExitScene()
    {
        SceneManager.LoadScene("Menu");
    }
}
