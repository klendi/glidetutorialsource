﻿using UnityEngine;
using System.Collections;

public class MenuCamera : MonoBehaviour
{
    public Mover mover;
    public Transform shopWaypoint;
    public Transform levelWaypoint;

    private Vector3 startPosition;
    private Quaternion startRotation;

    private Vector3 desiredPosition;
    private Quaternion desiredRotation;


    private void Start()
    {
        startPosition = desiredPosition = transform.localPosition;
        startRotation = desiredRotation = transform.rotation;
        mover = GetComponent<Mover>();
        mover.enabled = false;
    }

    private void Update()
    {
        transform.localPosition = Vector3.Lerp(transform.localPosition, desiredPosition, 0.05f);
        transform.localRotation = Quaternion.Lerp(transform.rotation, desiredRotation, 0.05f);
    }

    public void BackToMainMenu()
    {
        desiredPosition = startPosition;
        desiredRotation = startRotation;
        mover = GetComponent<Mover>();
        mover.enabled = true;
    }

    public void MoveToShop()
    {
        mover = GetComponent<Mover>();
        mover.enabled = false;
        desiredPosition = shopWaypoint.localPosition;
        desiredRotation = shopWaypoint.localRotation;
    }

    public void MoveToLevel()
    {
        mover = GetComponent<Mover>();
        mover.enabled = false;
        desiredPosition = levelWaypoint.localPosition;
        desiredRotation = levelWaypoint.localRotation;
    }
}
