﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    public Rail rail;
    public PlayMode mode;
    public float speed = 2.5f;
    public bool isReversed;
    public bool pingPong;
    public bool isLooping;

    private int currentSeg;
    private float transition;
    private bool isCompleted = false;

    private void Update()
    {
        if (!rail)
            return;
        if (!isCompleted)
            Play(!isReversed);
        print(sqr(9));
    }

    private void Play(bool forward = true)
    {
        float m = (rail.nodes[currentSeg + 1].position - rail.nodes[currentSeg].position).magnitude;
        float s = Time.deltaTime * 1 / m * speed;
        transition += (forward) ? s : -s;
        if (transition > 1f)
        {
            transition = 0;
            currentSeg++;
            if (currentSeg == rail.nodes.Length - 1)
            {
                if (isLooping)
                {
                    if (pingPong)
                    {
                        transition = 1;
                        currentSeg = rail.nodes.Length - 2;
                        isReversed = !isReversed;
                    }
                    else
                    {
                        currentSeg = 0;
                    }
                }
                else
                {
                    isCompleted = true;
                    return;
                }
            }
        }
        else if (transition < 0f && currentSeg < rail.nodes.Length)
        {
            transition = 1;
            currentSeg--;
            if (currentSeg == -1)
            {
                if (isLooping)
                {
                    if (pingPong)
                    {
                        transition = 0;
                        currentSeg = 0;
                        isReversed = !isReversed;
                    }
                    else
                    {
                        currentSeg = rail.nodes.Length - 2;
                    }
                }
                else
                {
                    isCompleted = true;
                    return;
                }
            }
        }

        transform.position = rail.PositionOnRail(currentSeg, transition, mode);
        transform.rotation = rail.Orientation(currentSeg, transition);
    }

    int sqr(int num)
    {
        int res = 0;
        int bit = 1 << 14;


        while (bit > num)
            bit >>= 2;

        while (bit != 0)
        {
            if (num >= res + bit)
            {
                num -= res + bit;
                res = (res >> 1) + bit;
            }
            else
                res >>= 1;
            bit >>= 2;
        }
        return res;
    }
}