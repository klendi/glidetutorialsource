﻿using UnityEngine;
using System.Collections;
using System.IO;

public class SaveManager : MonoBehaviour
{
    public static SaveManager Instance { get; set; }
    public SaveState state;
    private string filename = "sublex_data.sublex";
    private string directory = @"C:\Users\Klendi Gocci\Documents\Sublex Games\";
    byte[] keyBytes = { 145, 12, 32, 245, 98, 132, 98, 214, 6, 77, 131, 44, 221, 3, 9, 50 };

    public void Awake()
    {
        //ResetSave();
        DontDestroyOnLoad(gameObject);
        Instance = this;
        Load();
    }

    //Save the whole state of this saveState script to this player pref
    public void Save()
    {
        //PlayerPrefs.SetString("save", Helper.Serialize<SaveState>(state));
        //File.WriteAllText(directory + filename, Helper.Serialize(state));
        Encrypt();
    }

    public void Encrypt()
    {
        Klendi_Encrypt.KlendiEncryptPro.EncryptText(directory + filename, Helper.Serialize(state), keyBytes, keyBytes);
    }

    //Load the previous saved state from the player pref
    public void Load()
    {
        //check if we have a saved data
        //if (PlayerPrefs.HasKey("save"))
        //{
        //    state = Helper.DeSerialize<SaveState>(PlayerPrefs.GetString("save"));
        //}
        //else
        //{
        //    state = new SaveState();
        //    Save();
        //    print("No save data found, Creating a new one!");
        //}
        if (File.Exists(directory + filename))
        {
            string data = Klendi_Encrypt.KlendiEncryptPro.DecryptText(directory + filename, keyBytes, keyBytes);
            state = Helper.DeSerialize<SaveState>(data);
            print("File found! Decryption succes");
        }
        else
        {
            state = new SaveState();
            Save();
            print("No data found, creating new one");
        }
    }

    //Check if the color is owned
    public bool IsColorOwned(int index)
    {
        //check if the bit is set, so the color is owned
        return (state.colorOwned & (1 << index)) != 0;
    }

    //Check if the trail is owned
    public bool IsTrailOwned(int index)
    {
        //check if the bit is set, so the trail is owned
        return (state.trailOwned & (1 << index)) != 0;
    }

    //Attempt to buy a color,return true or false
    public bool BuyColor(int index, int cost)
    {
        if (state.gold >= cost)
        {
            //We have money, remove the current gold stack
            state.gold -= cost;
            UnlockColor(index);

            //Save the progress
            Save();

            return true;
        }
        else
        {
            //We dont have enough money, return false
            return false;
        }
    }

    //Attempt to buy a trail, return true or false
    public bool BuyTrail(int index, int cost)
    {
        if (state.gold >= cost)
        {
            //We have money, remove the current gold stack
            state.gold -= cost;
            UnlockTrail(index);

            //Save the progress
            Save();

            return true;
        }
        else
        {
            //We dont have enough money, return false
            return false;
        }
    }

    //Unlock a color in the "colorOwned" int
    public void UnlockColor(int index)
    {
        //Toggle on the bit at index
        state.colorOwned |= 1 << index;

    }

    //Unlock a trail in the "trailOwned" int
    public void UnlockTrail(int index)
    {
        //Toggle on the bit at index
        state.trailOwned |= 1 << index;

    }

    //Complete level
    public void CompleteLevel(int index)
    {
        //if this is the current active level
        if (state.completedLevel == index)
        {
            state.completedLevel++;
            Save();
        }
    }

    //Reset the whole save file
    public void ResetSave()
    {
        //PlayerPrefs.DeleteKey("save");
        File.Delete(directory + filename);
    }
}
