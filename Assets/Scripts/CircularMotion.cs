﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularMotion : MonoBehaviour
{
    float timeCounter = 0;
    public float speed;
    public float height;
    public float width;
    public float scale;

    private void Update()
    {
        timeCounter += Time.deltaTime * speed;

        float x = Mathf.Sin(timeCounter) * width;
        float y = Mathf.Sin(timeCounter) * height;

        transform.localScale = Vector3.one * scale;
        transform.position = new Vector3(x, y);
    }
}