﻿using UnityEngine;
using System.Collections;

public class Preloader : MonoBehaviour
{
    private CanvasGroup fadeGroup = new CanvasGroup();
    private float loadTime;
    private float minimunLoadTime = 5.0f; // minimum time of that scene

    private void Start()
    {
        //Grab the only canvas group

        fadeGroup = FindObjectOfType<CanvasGroup>();

        //start with a white screen

        fadeGroup.alpha = 1;

        //Preload the game
        //$$

        //Give a timestamp of the completiotion time
        //if loadtime is super, give it a small buffer so we can see the logo

        if (Time.time < minimunLoadTime)
            loadTime = minimunLoadTime;
        else
            loadTime = Time.time;
    }

    private void Update()
    {
        // Fade-in
        if (Time.time < minimunLoadTime)
        {
            fadeGroup.alpha = 2.5f - Time.time;
        }

        //Fade-out

        if (Time.time > minimunLoadTime && loadTime != 0)
        {
            fadeGroup.alpha = Time.time - minimunLoadTime;

            if (fadeGroup.alpha >= 1)
            {
                Application.LoadLevel("Menu");
            }
        }
    }
}
