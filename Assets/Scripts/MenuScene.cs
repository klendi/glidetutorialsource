﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScene : MonoBehaviour
{
    private CanvasGroup fadeGroup = new CanvasGroup();

    //this fadeInSpeed is so bcs we multiply it by Time.time, 
    //so if we want 10 seconds we say 0.1f
    private float fadeInSpeed = 0.33f;

    public RectTransform menuContainer;
    public Transform levelPanel;
    public Transform colorPanel, trailPanel;

    private Vector3 desiredMenuPosition;

    public Text colorBuySetText;
    public Text trailBuySetText;
    public Text goldText;

    private GameObject currentTrail;

    private MenuCamera menuCam;

    private int[] colorCosts = new int[] { 0, 10, 13, 15, 21, 19, 11, 19, 9, 10 };
    private int[] trailCosts = new int[] { 0, 50, 55, 66, 70, 150, 190, 190, 50, 120 };
    private int selectedColorIndex;
    private int selectedTrailIndex;
    private int activeColorIndex;
    private int activeTrailIndex;

    public AnimationCurve enteringLevelZoomCurve;
    private bool IsEnteringLevel = false;
    private float zoomDuration = 1f;
    private float zoomTransition;

    private void Start()
    {
        //Find the only menuCamera and assign it
        menuCam = FindObjectOfType<MenuCamera>();

        //Testing
        SaveManager.Instance.state.gold = 999;

        //Position our camera on the focus menu
        SetCameraTo(Manager.Instance.menuFocus);

        //Tell our gold text how much we should display
        UpdateGoldText();

        //Grab the only canvas group
        fadeGroup = FindObjectOfType<CanvasGroup>();

        //start with a white screen
        fadeGroup.alpha = 1;

        //set the beggining view to main menu
        NavigateTo(2);

        //add buton-click events to Shop
        InitShop();

        //add buton-click events to levels
        InitLevel();
    }

    private void Update()
    {
        //Fade-in
        //by default it last 3 seconds
        fadeGroup.alpha = 1 - Time.timeSinceLevelLoad * fadeInSpeed;

        // Menu Nav , Smooth one
        menuContainer.anchoredPosition3D = Vector3.Slerp(menuContainer.anchoredPosition3D, desiredMenuPosition, 0.1f);

        //Entering level zoom
        if (IsEnteringLevel)
        {
            //Add to the zoom transition float
            zoomTransition += (1 / zoomDuration) * Time.deltaTime;

            //Change the scale, following the animation curve
            menuContainer.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 2,enteringLevelZoomCurve.Evaluate(zoomTransition));

            //Change to the desired position of the canvas
            //This zoom in the center
            Vector3 newDesiredPosition = desiredMenuPosition * 2;

            //This adds to the specific position of the level on canvas
            RectTransform rt = levelPanel.GetChild(Manager.Instance.currentLevel).GetComponent<RectTransform>();
            newDesiredPosition -= rt.anchoredPosition3D * 2;

            //This line will override the previous position update
            menuContainer.anchoredPosition3D = Vector3.Lerp(desiredMenuPosition, newDesiredPosition, enteringLevelZoomCurve.Evaluate(zoomTransition));

            //Fade to white screen, this will override the first line of code of update
            fadeGroup.alpha = zoomTransition;

            //If we are done with fade
            if(zoomTransition >= 1)
            {
                //Enter the level
                SceneManager.LoadScene("Game");
            }


        }

    }


    private void InitShop()
    {
        //we make sure that we have assigned the references

        if (colorPanel == null || trailPanel == null)
            Debug.LogError("You didn't assigned the values to the color/trail set");

        //For every children transform under our color panel, find the button and add onclick()

        int i = 0;

        foreach (Transform t in colorPanel)
        {
            int currentIndex = i;

            Button b = t.GetComponent<Button>();

            b.onClick.AddListener(() => OnColorSelect(currentIndex));

            //Set the color of image based on if owned or not
            Image img = t.GetComponent<Image>();
            img.color = SaveManager.Instance.IsColorOwned(i)
                ? Manager.Instance.playerColors[currentIndex]
                : Color.Lerp(Manager.Instance.playerColors[currentIndex], new Color(0f, 0f, 0f, 0.7f), 0.75f);


            i++;
        }

        //reset the index
        i = 0;

        //we do the same for the trailpanel

        foreach (Transform t in trailPanel)
        {
            int currentIndex = i;


            Button b = t.GetComponent<Button>();

            b.onClick.AddListener(() => OnTrailSelect(currentIndex));

            i++;
        }

    }

    private void InitLevel()
    {
        if (levelPanel == null)
            Debug.LogError("You didn't assigned the values to the levelPanel Component");

        //For every children transform under our level panel, find the button and add onclick()

        int i = 0;

        foreach (Transform t in levelPanel)
        {
            int currentIndex = i;

            Button b = t.GetComponent<Button>();
            if (b != null)
            {
                b.onClick.AddListener(() => OnLevelSelect(currentIndex));

                Image img = t.GetComponent<Image>();

                //Is it unlocked?
                if (i <= SaveManager.Instance.state.completedLevel)
                {
                    //It is unlocked!
                    if (i == SaveManager.Instance.state.completedLevel)
                    {
                        //Its not completed
                        img.color = Color.white;
                    }
                    else
                    {
                        //Level is already completed
                        img.color = Color.green;
                    }
                }
                else
                {
                    //Level isn't unlocked disable the button
                    b.interactable = false;

                    //Set to dark color
                    img.color = Color.grey;
                }

                i++;
            }
        }
    }


    private void SetCameraTo(int menuIndex)
    {
        switch (menuIndex)
        {
            //0 and Default case = Level Menu
            default:
            case 0:
                desiredMenuPosition = Vector3.zero;
                break;

            //2 = Main Menu
            case 2:
                desiredMenuPosition = -Vector3.right * 1280.0f;
                break;

            //3 = Shop Menu
            case 3:
                desiredMenuPosition = 2 * (-Vector3.right * 1280.0f);
                break;
        }
    }

    private void NavigateTo(int menuIndex)
    {
        switch (menuIndex)
        {
            //0 and Default case = Level Menu
            default:
            case 0:
                desiredMenuPosition = Vector3.zero;
                menuCam.MoveToLevel();
                break;

            //2 = Main Menu
            case 2:
                desiredMenuPosition = -Vector3.right * 1280.0f;
                menuCam.BackToMainMenu();
                break;

            //3 = Shop Menu
            case 3:
                desiredMenuPosition = 2 * (-Vector3.right * 1280.0f);
                menuCam.MoveToShop();
                break;
        }
    }


    private void SetColor(int index)
    {
        activeColorIndex = index;

        //Change the color in the player model
        Manager.Instance.playerMaterial.color = Manager.Instance.playerColors[index];
        //Change buy/set button text
        colorBuySetText.text = "Current";
    }

    private void SetTrail(int index)
    {
        activeTrailIndex = index;

        //Change the trail of the ship
        if (currentTrail != null)
            Destroy(currentTrail);

        currentTrail = Instantiate(Manager.Instance.playerTrails[index]) as GameObject;

        //Set it as the children of the player
        currentTrail.transform.SetParent(FindObjectOfType<MenuPlayer>().transform);

        //Fix the scaling issues
        currentTrail.transform.localPosition = Vector3.zero;
        currentTrail.transform.localRotation = Quaternion.Euler(0, 0, 90);
        currentTrail.transform.localScale = Vector3.one * 0.01f;

        //Change buy/set button text
        trailBuySetText.text = "Current";
    }


    private void UpdateGoldText()
    {
        goldText.text = SaveManager.Instance.state.gold.ToString();
    }


    public void OnPlayClick()
    {
        NavigateTo(0);
    }

    public void OnShopClick()
    {
        NavigateTo(3);
    }

    public void OnBackClick()
    {
        NavigateTo(2);
    }


    private void OnColorSelect(int currentIndex)
    {
        //if the button clicked is already selected exit
        if (selectedColorIndex == currentIndex)
            return;

        //make the icon slighty bigger
        colorPanel.GetChild(currentIndex).GetComponent<RectTransform>().localScale = Vector3.one * 1.125f;

        //Put the previous one on normal scale
        colorPanel.GetChild(selectedColorIndex).GetComponent<RectTransform>().localScale = Vector3.one;

        //set the selected color
        selectedColorIndex = currentIndex;

        //Change the content of buy/set color, depending on the state of the color
        if (SaveManager.Instance.IsColorOwned(currentIndex))
        {
            //Color is owned
            //Is it already our current color?
            if(activeColorIndex == currentIndex)
            {
                colorBuySetText.text = "Current";
            }
            else
            {
                colorBuySetText.text = "Select";
            }

        }
        else
        {
            //Color isn't owned
            colorBuySetText.text = "Buy:" + colorCosts[currentIndex].ToString();
        }
    }

    private void OnTrailSelect(int currentIndex)
    {

        //if the button clicked is already selected exit
        if (selectedTrailIndex == currentIndex)
            return;

        //set the selected color
        selectedTrailIndex = currentIndex;

        //make the icon slighty bigger
        trailPanel.GetChild(currentIndex).GetComponent<RectTransform>().localScale = Vector3.one * 2.125f;

        //Put the previous one on normal scale
        trailPanel.GetChild(selectedTrailIndex).GetComponent<RectTransform>().localScale = Vector3.one;

        //Change the content of buy/set color, depending on the state of the color
        if (SaveManager.Instance.IsTrailOwned(currentIndex))
        {
            //Trail is owned
            //Is it already our current Trail?
            if (activeTrailIndex == currentIndex)
            {
                trailBuySetText.text = "Current";
            }
            else
            {
                trailBuySetText.text = "Select";
            }
        }
        else
        {
            //Color isn't owned
            trailBuySetText.text = "Buy: " + trailCosts[currentIndex].ToString();
        }
    }

    private void OnLevelSelect(int currentIndex)
    {
        Manager.Instance.currentLevel = currentIndex;
        //Application.LoadLevel("Game");
        IsEnteringLevel = true;
        print("Selectig level :" + currentIndex);
    }


    public void OnColorBuySet()
    {
        //Is the selected color owned
        if (SaveManager.Instance.IsColorOwned(selectedColorIndex))
        {
            //Set the color
            SetColor(selectedColorIndex);
        }
        else
        {
            //Attempt to buy the color
            if (SaveManager.Instance.BuyColor(selectedColorIndex, colorCosts[selectedColorIndex]))
            {
                //Success!
                SetColor(selectedColorIndex);
                //Change the color of button
                colorPanel.GetChild(selectedColorIndex).GetComponent<Image>().color = Manager.Instance.playerColors[selectedColorIndex];
                UpdateGoldText();
            }
            else
            {
                //Don't have enough gold
                //Play Sound FeedBack
                print("Not enough gold");
            }

        }
    }

    public void OnTrailBuySet()
    {
        //Is the selected trail owned
        if (SaveManager.Instance.IsTrailOwned(selectedTrailIndex))
        {
            //Set the trail
            SetTrail(selectedTrailIndex);
        }
        else
        {
            //Attempt to buy the trail
            if (SaveManager.Instance.BuyTrail(selectedTrailIndex, trailCosts[selectedTrailIndex]))
            {
                //Success!
                SetTrail(selectedTrailIndex);
                trailPanel.GetChild(selectedTrailIndex).GetComponent<Image>().color = Color.green;
                UpdateGoldText();
            }
            else
            {
                //Don't have enough gold
                //Play Sound FeedBack
                print("Not enough gold");
            }
        }
    }
}
