﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.IO.Compression;

namespace Klendi_Encrypt
{
    class KlendiEncrypt
    {
        public static byte[] Encrypt(byte[] data, byte[] key, byte[] iv)
        {
            using (Aes algorithm = Aes.Create())
            using (ICryptoTransform encryptor = algorithm.CreateEncryptor(key, iv))
                return Crypt(data, encryptor);
        }
        public static byte[] Decrypt(byte[] data, byte[] key, byte[] iv)
        {
            using (Aes algorithm = Aes.Create())
            using (ICryptoTransform decryptor = algorithm.CreateDecryptor(key, iv))
                return Crypt(data, decryptor);
        }
        private static byte[] Crypt(byte[] data, ICryptoTransform cryptor)
        {
            MemoryStream m = new MemoryStream();
            using (Stream c = new CryptoStream(m, cryptor, CryptoStreamMode.Write))
                c.Write(data, 0, data.Length);
            return m.ToArray();
        }

        private static string Encrypt(string data, byte[] key, byte[] iv)
        {
            return Convert.ToBase64String(
            Encrypt(Encoding.UTF8.GetBytes(data), key, iv));
        }
        private static string Decrypt(string data, byte[] key, byte[] iv)
        {
            return Encoding.UTF8.GetString(
            Decrypt(Convert.FromBase64String(data), key, iv));
        }

        public static void Encrypt(string path, string data, byte[] key, byte[] iv)
        {
            File.WriteAllText(path, Encrypt(data, key, iv));
        }
        public static void Decrypt(string path, string newFilePath, byte[] key, byte[] iv)
        {
            string generatedData = File.ReadAllText(path);
            File.WriteAllText(newFilePath, Decrypt(generatedData, key, iv));
        }

    }
    class KlendiEncryptPro
    {
        public static void EncryptText(string path, string data, byte[] key, byte[] iv)
        {
            byte[] generatedData = Encoding.UTF8.GetBytes(data);
            using (SymmetricAlgorithm algorithm = Aes.Create())
            using (ICryptoTransform encryptor = algorithm.CreateEncryptor(key, iv))
            using (Stream f = File.Create(path))
            using (Stream c = new CryptoStream(f, encryptor, CryptoStreamMode.Write))
                c.Write(generatedData, 0, data.Length);
        }
        public static void EncryptAny(string path, byte[] data, byte[] key, byte[] iv)
        {
            using (SymmetricAlgorithm algorithm = Aes.Create())
            using (ICryptoTransform encryptor = algorithm.CreateEncryptor(key, iv))
            using (Stream f = File.Create(path))
            using (Stream c = new CryptoStream(f, encryptor, CryptoStreamMode.Write))
                c.Write(data, 0, data.Length);
        }

        public static void DecryptText(string path, string newfilePath, byte[] key, byte[] iv)
        {
            string generatedData = "";
            List<byte> dataBytes = new List<byte>();
            byte[] byteData;

            using (SymmetricAlgorithm algorithm = Aes.Create())
            using (ICryptoTransform decryptor = algorithm.CreateDecryptor(key, iv))
            using (Stream f = File.OpenRead(path))
            using (Stream c = new CryptoStream(f, decryptor, CryptoStreamMode.Read))
                for (int i = 0; (i = c.ReadByte()) > -1; i++)
                {
                    dataBytes.Add((byte)i);
                }
            byteData = dataBytes.ToArray();
            generatedData = Encoding.UTF8.GetString(byteData);
            File.WriteAllText(newfilePath, generatedData);
        }
        public static string DecryptText(string path, byte[] key, byte[] iv)
        {
            string generatedData = "";
            List<byte> dataBytes = new List<byte>();
            byte[] byteData;

            using (SymmetricAlgorithm algorithm = Aes.Create())
            using (ICryptoTransform decryptor = algorithm.CreateDecryptor(key, iv))
            using (Stream f = File.OpenRead(path))
            using (Stream c = new CryptoStream(f, decryptor, CryptoStreamMode.Read))
                for (int i = 0; (i = c.ReadByte()) > -1; i++)
                {
                    dataBytes.Add((byte)i);
                }
            byteData = dataBytes.ToArray();
            generatedData = Encoding.UTF8.GetString(byteData);
            return generatedData;
            //File.WriteAllText(newfilePath, generatedData);
        }
        public static void DecryptAny(string fileToDecrypt, string newfilePath, byte[] key, byte[] iv)
        {
            List<byte> dataBytes = new List<byte>();
            byte[] byteData;

            using (SymmetricAlgorithm algorithm = Aes.Create())
            using (ICryptoTransform decryptor = algorithm.CreateDecryptor(key, iv))
            using (Stream f = File.OpenRead(fileToDecrypt))
            using (Stream c = new CryptoStream(f, decryptor, CryptoStreamMode.Read))
                for (int i = 0; (i = c.ReadByte()) > -1; i++)
                {
                    dataBytes.Add((byte)i);
                }
            byteData = dataBytes.ToArray();
            File.WriteAllBytes(newfilePath, byteData);
        }
    }
}
